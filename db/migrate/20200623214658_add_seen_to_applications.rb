class AddSeenToApplications < ActiveRecord::Migration[6.0]
  def change
    add_column :applications, :seen, :boolean, default: false
  end
end
