class RemoveUserFromJobs < ActiveRecord::Migration[6.0]
  def change
    remove_column :jobs, :user, :integer
  end
end
