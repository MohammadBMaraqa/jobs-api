class AddExtraInformationToApplications < ActiveRecord::Migration[6.0]
  def change
    add_column :applications, :extra_information, :string
  end
end
