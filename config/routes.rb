Rails.application.routes.draw do
  devise_for :users

  get 'home/index'
  
  resources :jobs do
  	resources :applications, except: [:index]
  end

  root 'home#index'

  

end
