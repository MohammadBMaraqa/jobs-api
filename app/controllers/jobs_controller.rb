class JobsController < ApplicationController
	before_action :find_job, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!, except: [:show, :index]

	def index
    	@job = Job.all
    	@application = Application.all
  	end

	def show 
		@application = Application.where(job_id: @job.id)
	end

	def new
		@job = Job.new
	end

	def edit
	end

	def create
		@job = Job.new(job_params)
		@job.user = current_user
		if @job.save
			redirect_to @job
		else
			render 'new'
		end
	end

	def update
		if @job.update(job_params)
			redirect_to @job
		else
			render 'edit'
		end
	end	

	def destroy
		@job.destroy
		redirect_to jobs_path
	end

private 

	def job_params
		params.require(:job).permit(:title, :description)
	end

	def find_job
		@job = Job.find(params[:id])
	end
end
