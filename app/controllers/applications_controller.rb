class ApplicationsController < ApplicationController
	before_action :find_application, only: [:show, :edit, :update, :destroy]
	before_action :find_job
	before_action :authenticate_user!

	def show
		if current_user.admin
			@application.seen = true
			@application.save
		end
	end

	def new
		@application = Application.new
	end

	def edit

	end

	def create
		@application = Application.new(application_params)
		@application.user_id = current_user.id
		@application.job_id = @job.id

		if @application.save
			redirect_to @job
		else
			render 'new'
		end
	end

	def update
		@application.update(application_params)
	end

	def destroy
		@application.destroy
		redirect_to jobs_path
	end

	private
		def find_application
			@application = Application.find(params[:id])
		end

		def find_job
			@job = Job.find(params[:job_id])
		end

		def application_params
			params.require(:application).permit(:extra_information)
		end

end
