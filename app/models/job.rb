class Job < ApplicationRecord
	
	belongs_to :user
	has_many :applications, dependent: :destroy
	 
	validates :title, :description, presence: true

end
